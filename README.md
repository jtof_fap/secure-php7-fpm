### Description
Docker container sources for secure php7.3-fpm server based on official php:7.3-fpm-stretch image

For `latest`, `7.3` and `7.1` tag :

 - Dist-upgrade on image
 - Compile and install **gd** and **mysqli** module
 - Default php:7.1-fpm configuration files (need to be secure)
 - Fix UID/GID to 3005 for user www-data

For `7.x-no-modules` tag:

 - idem without **gd** and **mysqli** modules

### Run

First, in your server create the following user:
    
    addgroup docker-www --gid 3005
    adduser --shell /bin/false --no-create-home --uid 3005 --gid 3005 --disabled-password --disabled-login docker-www
    chown -R 3005:3005 /location/DataDir/etc/php
    chown -R 3005:3005 /location/DataDir/log/php
    chown -R 3005:3005 /location/DataDir/www


Then, you could use a docker-compose.yml like :

    ...
      php:

	   # Common definition #
		image: secure-php:latest
		container_name: WEBSTACK-PHP
		restart: always

	   # Network definition #
		network_mode: "bridge"

	   # Start order and links
		depends_on:
		  - mariadb

		links:
		 - mariadb:mysql

	   # Security option definition #
		user: "3005:3005"

		security_opt:
		  - no-new-privileges

		cap_drop:
		  - ALL

	   # Volumes definition #
		volumes:
		  - /location/DataDir/etc/php:/usr/local/etc/
		  - /location/DataDir/www:/var/www/
		  - /location/DataDir/log/php:/var/log/php/
    ....
